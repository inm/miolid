
var mysql = require('mysql');
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: process.env.DBPASS,
  database: 'mobile'
});

connection.connect(function(err) {
  if (err) {
    console.error('error:', err);
  } else {
    var query = connection.query('select unix_timestamp(max(created)) from article', function(err, result) {
      if (err) {
        console.error('error:', err);
      } else {
        console.info('result:', result);
      }
      connection.end();
    });
    console.log(query.sql);
  }
});

